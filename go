#!/bin/bash

# lts/fermium -> v14.21.3
NODE_VERSION=v14.21.3

PWD_SRC=$(pwd)

# Cargar NVM
NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

update() {
	# ./blockly-config.bash
	nvm install $NODE_VERSION
	nvm use $NODE_VERSION
	cd $PWD_SRC/blockly/
	npm install
	# npm update #(de ser requerido hacer un npm audit fix)
	# npm audit fix
}

init() {
	git submodule init
	git submodule update
	echo "Actualizar otras dependencias"
	cd $PWD_SRC/catalejo-oursblockly/
	git pull ..
	update
}

config-package() {
	(cat <(head -n -1 ./blockly-package.json) <(tail -n +4 ./blockly/package.json)) >./package.json
	mv ./package.json ./blockly/package.json
}

config() {
	config-package
	cd $PWD_SRC/catalejo-generators/
	./configBlockly.bash
	./addChucKInBlockly.sh
}

build() {
	cd $PWD_SRC/blockly/
	nvm use $NODE_VERSION
	npm run build
}

pack() {
	cd $PWD_SRC/blockly/
	nvm use $NODE_VERSION
	npm run package
	cd $PWD_SRC/blockly/dist/
	npm publish --access public
}

help() {
	echo "
init: actualizar submódulos e instalar la versión de node
config: Agregar los ajustes de blockly con otros soportes
build: construir compilados de blockly
pack: empaquetar y subir a npm
  "
}

if [[ -v 1 ]]; then
	$1
else
	help
fi
