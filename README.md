# catalejo-blockly

## Clonar este repositorio

```bash
git clone git@gitlab.com:catalejo-team/catalejo-dev/catalejo-blockly.git
git submodule init
git submodule update
```

## Actualizar submodulos

```bash
cd catalejo-oursblockly/
git pull ..
```

## Cambiar package.json

```bash
./blockly-config.bash
```

## Compilar Blockly con nodejs

Versión de npm usada: fermium
```bash
nvm list
nvm use lts/fermium
```

```bash
cd ./blockly/
npm install
npm update      #(de ser requerido hacer un npm audit fix)
npm audit fix
npm run build
```

## Publicar Blockly en npmjs

Se requiere realizar el proceso de autenticación

```bash
cd blockly/
npm run package
cd dist/
npm publish --access public
```
