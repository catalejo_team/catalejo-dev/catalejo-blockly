# Página de prueba de bloques creados para chuck

Recordar que estos bloques fueron creados con la capacidad de
interactuar con variables con tipo.

![Ejemplo de variables con tipo](./tipos-chuck.png)

Lance el script para observar los resultados requeridos.

```bash
./init.bash
```

## Referencias

https://google.github.io/blockly-samples/plugins/typed-variable-modal/README.html

https://github.com/google/blockly-samples/tree/master/plugins/typed-variable-modal

https://audicle.cs.princeton.edu/mini/linux/
