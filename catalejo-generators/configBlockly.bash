#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	brief
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Friday 30 June 2023
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

cp -var ./scripts/gulpfiles/build_tasks.js ../blockly/scripts/gulpfiles/build_tasks.js
cp -var ./scripts/gulpfiles/chunks.json ../blockly/scripts/gulpfiles/chunks.json
cp -var ./scripts/gulpfiles/package_tasks.js ../blockly/scripts/gulpfiles/package_tasks.js
cp -var ./scripts/gulpfiles/test_tasks.js ../blockly/scripts/gulpfiles/test_tasks.js

cp -var ./scripts/migration/renamings.json5 ../blockly/scripts/migration/renamings.json5

cp -var ./scripts/package/chuck.js ../blockly/scripts/package/chuck.js

cp -var ./typings/chuck.d.ts ../blockly/typings/chuck.d.ts
