/**
 * @license
 * Copyright 2014 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating Chuck for loop blocks.
 */
'use strict';

goog.module('Blockly.Chuck.loops');

const {chuckGenerator: Chuck} = goog.require('Blockly.Chuck');
const stringUtils = goog.require('Blockly.utils.string');
const {NameType} = goog.require('Blockly.Names');


Chuck['controls_repeat_ext'] = function(block) {
  let repeats;
  // Repeat n times.
  if (block.getField('TIMES')) {
    // Internal number.
    repeats = String(Number(block.getFieldValue('TIMES')));
  } else {
    // External number.
    repeats = Chuck.valueToCode(block, 'TIMES', Chuck.ORDER_ASSIGNMENT) || '0';
  }
  let branch = Chuck.statementToCode(block, 'DO');
  branch = Chuck.addLoopTrap(branch, block);
  let code = '';
  const loopVar = Chuck.nameDB_.getDistinctName('count', NameType.VARIABLE);
  let endVar = repeats;
  if (!repeats.match(/^\w+$/) && !stringUtils.isNumber(repeats)) {
    endVar = Chuck.nameDB_.getDistinctName('repeat_end', NameType.VARIABLE);
    code += 'var ' + endVar + ' = ' + repeats + ';\n';
  }
  code += 'for ( 0 => int ' + loopVar + '; ' + loopVar + ' < ' + endVar + '; ' +
      loopVar + '++)\n{\n' + branch + '}\n';
  return code;
};

Chuck['controls_repeat'] = Chuck['controls_repeat_ext'];

Chuck['controls_whileUntil'] = function(block) {
  // Do while/until loop.
  const until = block.getFieldValue('MODE') === 'UNTIL';
  let argument0 =
      Chuck.valueToCode(
          block, 'BOOL', until ? Chuck.ORDER_UNARY_PREFIX : Chuck.ORDER_NONE) ||
      'false';
  let branch = Chuck.statementToCode(block, 'DO');
  branch = Chuck.addLoopTrap(branch, block);
  if (until) {
    argument0 = '!' + argument0;
  }
  return 'while (' + argument0 + ')\n{\n' + branch + '}\n';
};

Chuck['controls_for'] = function(block) {
  // For loop.
  const variable0 =
      Chuck.nameDB_.getName(block.getFieldValue('VAR'), NameType.VARIABLE);
  const argument0 =
      Chuck.valueToCode(block, 'FROM', Chuck.ORDER_ASSIGNMENT) || '0';
  const argument1 = Chuck.valueToCode(block, 'TO', Chuck.ORDER_ASSIGNMENT) || '0';
  const increment = Chuck.valueToCode(block, 'BY', Chuck.ORDER_ASSIGNMENT) || '1';
  let branch = Chuck.statementToCode(block, 'DO');
  branch = Chuck.addLoopTrap(branch, block);
  let code;
  if (stringUtils.isNumber(argument0) && stringUtils.isNumber(argument1) &&
      stringUtils.isNumber(increment)) {
    // All arguments are simple numbers.
    const up = Number(argument0) <= Number(argument1);
    code = 'for (' + variable0 + ' = ' + argument0 + '; ' + variable0 +
        (up ? ' <= ' : ' >= ') + argument1 + '; ' + variable0;
    const step = Math.abs(Number(increment));
    if (step === 1) {
      code += up ? '++' : '--';
    } else {
      code += (up ? ' +=> ' : ' -=> ') + step;
    }
    code += ') {\n' + branch + '}\n';
  } else {
    code = '';
    // Cache non-trivial values to variables to prevent repeated look-ups.
    let startVar = argument0;
    if (!argument0.match(/^\w+$/) && !stringUtils.isNumber(argument0)) {
      startVar =
          Chuck.nameDB_.getDistinctName(variable0 + '_start', NameType.VARIABLE);
      code += argument0 + ' => int ' + startVar + ';\n';
    }
    let endVar = argument1;
    if (!argument1.match(/^\w+$/) && !stringUtils.isNumber(argument1)) {
      endVar =
          Chuck.nameDB_.getDistinctName(variable0 + '_end', NameType.VARIABLE);
      code += argument1 + ' => int ' + endVar + ';\n';
    }
    // Determine loop direction at start, in case one of the bounds
    // changes during loop execution.
    const incVar =
        Chuck.nameDB_.getDistinctName(variable0 + '_inc', NameType.VARIABLE);
    if (stringUtils.isNumber(increment)) {
      code += Math.abs(increment);
    } else {
      code += 'Std.abs(' + increment +')';
    }
    code += ' => int ' + incVar +';\n';
    code += 'if (' + startVar + ' > ' + endVar + ') {\n';
    code += Chuck.INDENT + '-' + incVar + ' => ' + incVar + ';\n';
    code += '}\n';
    code += 'for (' + startVar + ' => ' + variable0 + '; ' + incVar +
        ' >= 0 ? ' + variable0 + ' <= ' + endVar + ' : ' + variable0 +
        ' >= ' + endVar + '; ' + variable0 + ' +=> ' + incVar + ') {\n' +
        branch + '}\n';
  }
  return code;
};

Chuck['controls_for_chuck'] = function(block) {
  // For loop.
  const variable0 =
      Chuck.valueToCode(block, 'VAR', Chuck.ORDER_ATOMIC);
  const argument0 =
      Chuck.valueToCode(block, 'FROM', Chuck.ORDER_ASSIGNMENT) || '0';
  const argument1 = Chuck.valueToCode(block, 'TO', Chuck.ORDER_ASSIGNMENT) || '0';
  const increment = Chuck.valueToCode(block, 'BY', Chuck.ORDER_ASSIGNMENT) || '1';
  let branch = Chuck.statementToCode(block, 'DO');
  branch = Chuck.addLoopTrap(branch, block);
  let code;
  if (stringUtils.isNumber(argument0) && stringUtils.isNumber(argument1) &&
      stringUtils.isNumber(increment)) {
    // All arguments are simple numbers.
    const up = Number(argument0) <= Number(argument1);
    code = 'for (' + argument0 + ' => ' + variable0 + '; ' + variable0 +
        (up ? ' <= ' : ' >= ') + argument1 + '; ';
    const step = Math.abs(Number(increment));
    if (step === 1) {
      code += variable0 + (up ? '++' : '--');
    } else {
      code += step + (up ? ' +=> ' : ' -=> ') + variable0;
    }
    code += ') {\n' + branch + '}\n';
  } else {
    code = '';
    // Cache non-trivial values to variables to prevent repeated look-ups.
    let startVar = argument0;
    if (!argument0.match(/^\w+$/) && !stringUtils.isNumber(argument0)) {
      startVar =
          Chuck.nameDB_.getDistinctName(variable0 + '_start', NameType.VARIABLE);
      code += argument0 + ' => int ' + startVar + ';\n';
    }
    let endVar = argument1;
    if (!argument1.match(/^\w+$/) && !stringUtils.isNumber(argument1)) {
      endVar =
          Chuck.nameDB_.getDistinctName(variable0 + '_end', NameType.VARIABLE);
      code += argument1 + ' => int ' + endVar + ';\n';
    }
    // Determine loop direction at start, in case one of the bounds
    // changes during loop execution.
    const incVar =
        Chuck.nameDB_.getDistinctName(variable0 + '_inc', NameType.VARIABLE);
    if (stringUtils.isNumber(increment)) {
      code += Math.abs(increment);
    } else {
      code += 'Std.abs(' + increment +')';
    }
    code += ' => int ' + incVar +';\n';
    code += 'if (' + startVar + ' > ' + endVar + ') {\n';
    code += Chuck.INDENT + '-' + incVar + ' => ' + incVar + ';\n';
    code += '}\n';
    code += 'for (' + startVar + ' => ' + variable0 + '; ' + incVar +
        ' >= 0 ? ' + variable0 + ' <= ' + endVar + ' : ' + variable0 +
        ' >= ' + endVar + '; ' + incVar + ' +=> ' + variable0 + ') {\n' +
        branch + '}\n';
  }
  return code;
};

Chuck['controls_forEach'] = function(block) {
  // For each loop.
  const variable0 =
      Chuck.nameDB_.getName(block.getFieldValue('VAR'), NameType.VARIABLE);
  const argument0 =
      Chuck.valueToCode(block, 'LIST', Chuck.ORDER_ASSIGNMENT) || '[]';
  let branch = Chuck.statementToCode(block, 'DO');
  branch = Chuck.addLoopTrap(branch, block);
  const code =
      'for (var ' + variable0 + ' in ' + argument0 + ') {\n' + branch + '}\n';
  return code;
};

Chuck['controls_flow_statements'] = function(block) {
  // Flow statements: continue, break.
  let xfix = '';
  if (Chuck.STATEMENT_PREFIX) {
    // Automatic prefix insertion is switched off for this block.  Add manually.
    xfix += Chuck.injectId(Chuck.STATEMENT_PREFIX, block);
  }
  if (Chuck.STATEMENT_SUFFIX) {
    // Inject any statement suffix here since the regular one at the end
    // will not get executed if the break/continue is triggered.
    xfix += Chuck.injectId(Chuck.STATEMENT_SUFFIX, block);
  }
  if (Chuck.STATEMENT_PREFIX) {
    const loop = block.getSurroundLoop();
    if (loop && !loop.suppressPrefixSuffix) {
      // Inject loop's statement prefix here since the regular one at the end
      // of the loop will not get executed if 'continue' is triggered.
      // In the case of 'break', a prefix is needed due to the loop's suffix.
      xfix += Chuck.injectId(Chuck.STATEMENT_PREFIX, loop);
    }
  }
  switch (block.getFieldValue('FLOW')) {
    case 'BREAK':
      return xfix + 'break;\n';
    case 'CONTINUE':
      return xfix + 'continue;\n';
  }
  throw Error('Unknown flow statement.');
};
