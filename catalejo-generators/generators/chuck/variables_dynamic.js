/**
 * @license
 * Copyright 2018 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating Chuck for dynamic variable blocks.
 */
'use strict';

goog.module('Blockly.Chuck.variablesDynamic');

const {chuckGenerator: Chuck} = goog.require('Blockly.Chuck');
/** @suppress {extraRequire} */
goog.require('Blockly.Chuck.variables');


// Chuck is dynamically typed.
Chuck['variables_get_dynamic'] = Chuck['variables_get'];
Chuck['variables_set_dynamic'] = Chuck['variables_set'];
