#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	script de instalación de chuck en google
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Sunday 11 April 2021
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

PATH_BUILD_SCRIP="../blockly/scripts/gulpfiles"

# if [[ ! -f ./build_tasks_official.js ]]; then
#   printf "${GREEN}Creando copia de build_tasks oficial ${NC}\n"
#   cp $PATH_BUILD_SCRIP/build_tasks.js ./build_tasks_official.js
#   printf "${CYAN}exportando el archivo build_tasks a blockly${NC}\n"
#   cp -f ./build_tasks.js $PATH_BUILD_SCRIP/build_tasks.js
# else
#   printf "Ya existe una copia de build_tasks_official.js, si deseas borrarla debes hacerlo manualmente\n"
#   printf "${CYAN}exportando el archivo build_tasks a blockly${NC}\n"
#   cp -f ./build_tasks.js $PATH_BUILD_SCRIP/build_tasks.js
# fi

PATH_GENERATORS="../blockly/generators"

if [[ ! -L $PATH_GENERATORS/chuck ]]; then
  ln -sr ./generators/chuck $PATH_GENERATORS/chuck
else
  rm -f $PATH_GENERATORS/chuck
  ln -sr ./generators/chuck $PATH_GENERATORS/chuck
fi

if [[ ! -L $PATH_GENERATORS/chuck.js ]]; then
    ln -sr ./generators/chuck.js $PATH_GENERATORS/chuck.js
else
  rm -f $PATH_GENERATORS/chuck.js
  ln -sr ./generators/chuck.js $PATH_GENERATORS/chuck.js
fi

PATH_BLOCKS="../blockly"

if [[ ! -L $PATH_BLOCKS/blocks ]]; then
  mv $PATH_BLOCKS/blocks $PATH_BLOCKS/blocks_old
  ln -sr ./blocks $PATH_BLOCKS/blocks
fi
