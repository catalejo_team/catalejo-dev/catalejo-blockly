/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Chuck generator module; just a wrapper for chuck_compressed.js.
 */
